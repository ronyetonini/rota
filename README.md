# Aplicação Rota #

**Algumas APIs utilizadas:**

* Spring boot - Auto inicio da plataforma Spring com servlet container
* flyway - Controle de versionamento de banco de dados
* Gson - Conversão de Java Objects em JSON e vice versa
* H2 DB - Banco de dados embedded

**Requisitos:**

* Java 8
* Maven

**Preparação para o funcionamento:**

* Compilar o projeto

```
#!command

Entre na pasta ./Rota 
Execute:

mvn package
```



* Execute o projeto:

	
```
#!command

java -jar target\rota-0.0.1-SNAPSHOT.jar
```


*Observações
O banco de dados H2DB é automaticamente criado e já com uma carga inicial de dados que pode ser facilmente substituído via serviço "cargaMapa"*



**Serviços:**

* Toda comunicação é formatada em JSON.
* Para calculo da rota foi utilizado o algoritmo de Dijkstra.


```
#!http

GET http://localhost:8080/services/melhorRota
POST http://localhost:8080/services/cargaMapa
```



**Exemplo de parвmetro do serviço "melhorRota"**


```
#!http

?mapa=SP&origem=A&destino=D&autonomia=10&valorLitroCombustivel=2.5
```



**Exemplo de request para o serviço "cargaMapa"**


```
#!json

{
   "nome":"SP",
   "rotas":[
      {
         "origem":"A",
         "destino":"B",
         "distancia":5
      },
      {
         "origem":"A",
         "destino":"C",
         "distancia":10
      },
      {
         "origem":"B",
         "destino":"D",
         "distancia":3
      },
      {
         "origem":"B",
         "destino":"G",
         "distancia":10
      },
      {
         "origem":"C",
         "destino":"D",
         "distancia":10
      },
      {
         "origem":"C",
         "destino":"E",
         "distancia":10
      },
      {
         "origem":"D",
         "destino":"F",
         "distancia":10
      },
      {
         "origem":"D",
         "destino":"E",
         "distancia":5
      },
      {
         "origem":"E",
         "destino":"F",
         "distancia":4
      },
      {
         "origem":"E",
         "destino":"H",
         "distancia":1
      },
      {
         "origem":"G",
         "destino":"F",
         "distancia":10
      },
      {
         "origem":"H",
         "destino":"F",
         "distancia":1
      }
   ]
}
```


**Repositorio publico dos fontes:**

Os fontes estao localizados do repositorio do bibucket
http://bitbucket.org/ronyetonini/rota
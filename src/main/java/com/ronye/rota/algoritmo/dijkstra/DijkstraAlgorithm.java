package com.ronye.rota.algoritmo.dijkstra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.util.StringUtils;

import com.ronye.rota.domain.Rota;

public class DijkstraAlgorithm {

	private final List<Vertex> nodes;
	private final List<Edge> edges;
	private Set<Vertex> settledNodes;
	private Set<Vertex> unSettledNodes;
	private Map<Vertex, Vertex> predecessors;
	private Map<Vertex, Integer> distance;

	public DijkstraAlgorithm(Set<Rota> rotas) {
		if (rotas == null || rotas.isEmpty()) {
			throw new IllegalArgumentException("Para processar a menor rota é necessário rotas");
		}
		List<Vertex> nodesAux = new ArrayList<Vertex>();
		List<Edge> edgesAux = new ArrayList<Edge>();
		
		Set<Vertex> aux = new HashSet<Vertex>();
		
		for (Iterator<Rota> iterator = rotas.iterator(); iterator.hasNext();) {
			Rota rota = (Rota) iterator.next();
			Vertex location = new Vertex(rota.getOrigem(), rota.getOrigem());
			aux.add(location);
			Vertex location2 = new Vertex(rota.getDestino(), rota.getDestino());
			aux.add(location2);
		}
		nodesAux.addAll(aux);
		
		for (Iterator<Rota> iterator = rotas.iterator(); iterator.hasNext();) {
			Rota rota = (Rota) iterator.next();
			addLane(edgesAux, nodesAux, rota.getId().toString(), rota.getOrigem(), rota.getDestino(), rota.getDistancia(), rota);
		}

		Graph graph = new Graph(nodesAux, edgesAux);
		this.nodes = new ArrayList<Vertex>(graph.getVertexes());
		this.edges = new ArrayList<Edge>(graph.getEdges());
	}

	public DijkstraAlgorithm(Graph graph) {
		// create a copy of the array so that we can operate on this array
		this.nodes = new ArrayList<Vertex>(graph.getVertexes());
		this.edges = new ArrayList<Edge>(graph.getEdges());
	}

	private void addLane(List<Edge> edgesAux, List<Vertex> nodesAux, String laneId, String sourceLocNo, String destLocNo, int duration, Rota rota) {
		Edge lane = new Edge(laneId, findVertexById(nodesAux, sourceLocNo), findVertexById(nodesAux, destLocNo), duration, rota);
		edgesAux.add(lane);
	}
	private Vertex findVertexById(List<Vertex> nodesAux, String id){
		for (Vertex vertex : nodesAux) {
			if (id.equals(vertex.getId())){
				return vertex;
			}
		}
		return null;
	}
	
	public void execute(String source) {
		execute(findVertexById(nodes, source));
	}

	public void execute(Vertex source) {
		if (StringUtils.isEmpty(source)) {
			throw new IllegalArgumentException("Para processar a menor rota é necessário origem e destino");
		}
		settledNodes = new HashSet<Vertex>();
		unSettledNodes = new HashSet<Vertex>();
		distance = new HashMap<Vertex, Integer>();
		predecessors = new HashMap<Vertex, Vertex>();
		distance.put(source, 0);
		unSettledNodes.add(source);
		while (unSettledNodes.size() > 0) {
			Vertex node = getMinimum(unSettledNodes);
			settledNodes.add(node);
			unSettledNodes.remove(node);
			findMinimalDistances(node);
		}
	}
	
	private void findMinimalDistances(Vertex node) {
		List<Vertex> adjacentNodes = getNeighbors(node);
		for (Vertex target : adjacentNodes) {
			if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node, target)) {
				distance.put(target, getShortestDistance(node) + getDistance(node, target));
				predecessors.put(target, node);
				unSettledNodes.add(target);
			}
		}

	}

	private int getDistance(Vertex node, Vertex target) {
		for (Edge edge : edges) {
			if (edge.getSource().equals(node) && edge.getDestination().equals(target)) {
				return edge.getWeight();
			}
		}
		throw new RuntimeException("Should not happen");
	}

	private List<Vertex> getNeighbors(Vertex node) {
		List<Vertex> neighbors = new ArrayList<Vertex>();
		for (Edge edge : edges) {
			if (edge.getSource().equals(node) && !isSettled(edge.getDestination())) {
				neighbors.add(edge.getDestination());
			}
		}
		return neighbors;
	}

	private Vertex getMinimum(Set<Vertex> vertexes) {
		Vertex minimum = null;
		for (Vertex vertex : vertexes) {
			if (minimum == null) {
				minimum = vertex;
			} else {
				if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
					minimum = vertex;
				}
			}
		}
		return minimum;
	}

	private boolean isSettled(Vertex vertex) {
		return settledNodes.contains(vertex);
	}

	private int getShortestDistance(Vertex destination) {
		Integer d = distance.get(destination);
		if (d == null) {
			return Integer.MAX_VALUE;
		} else {
			return d;
		}
	}

	/*
	 * This method returns the path from the source to the selected target and
	 * NULL if no path exists
	 */
	public LinkedList<Vertex> getPath(Vertex target) {
		LinkedList<Vertex> path = new LinkedList<Vertex>();
		Vertex step = target;
		// check if a path exists
		if (predecessors.get(step) == null) {
			return null;
		}
		path.add(step);
		while (predecessors.get(step) != null) {
			step = predecessors.get(step);
			path.add(step);
		}
		// Put it into the correct order
		Collections.reverse(path);
		return path;
	}

	/*
	 * This method returns the path from the source to the selected target and
	 * NULL if no path exists
	 */
	public LinkedList<Edge> getPathEdges(String target) {
		if (StringUtils.isEmpty(target)) {
			throw new IllegalArgumentException("Para processar a menor rota é necessário origem e destino");
		}
		return getPathEdges(findVertexById(nodes, target));
	}
	
	public LinkedList<Edge> getPathEdges(Vertex target) {
		LinkedList<Edge> path = new LinkedList<Edge>();
		Vertex step = target;
		// check if a path exists
		if (predecessors.get(step) == null) {
			return null;
		}
		while (predecessors.get(step) != null) {
			path.add(findEdge(predecessors.get(step), step));
			step = predecessors.get(step);
		}
		// Put it into the correct order
		Collections.reverse(path);
		return path;
	}
	
	private Edge findEdge(Vertex origem, Vertex destino) {
		for (Edge edge : edges) {
			if (edge.getSource().equals(origem) && edge.getDestination().equals(destino)) {
				return edge;
			}
		}
		return null;
	}
}

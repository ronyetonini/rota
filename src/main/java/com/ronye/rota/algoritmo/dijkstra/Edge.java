package com.ronye.rota.algoritmo.dijkstra;

import com.ronye.rota.domain.Rota;

public class Edge {
	private final String id;
	private final Vertex source;
	private final Vertex destination;
	private final int weight;
	private final Rota rota;

	public Edge(String id, Vertex source, Vertex destination, int weight, Rota rota) {
		this.id = id;
		this.source = source;
		this.destination = destination;
		this.weight = weight;
		this.rota = rota;
	}

	public String getId() {
		return id;
	}

	public Vertex getDestination() {
		return destination;
	}

	public Vertex getSource() {
		return source;
	}

	public int getWeight() {
		return weight;
	}


	public Rota getRota() {
		return rota;
	}

	@Override
	public String toString() {
		return source + " " + destination;
	}

}
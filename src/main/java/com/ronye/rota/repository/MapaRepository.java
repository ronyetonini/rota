package com.ronye.rota.repository;

import org.springframework.data.repository.CrudRepository;

import com.ronye.rota.domain.Mapa;

public interface MapaRepository extends CrudRepository<Mapa, Long> {
	Mapa findByNome(String nome);
}

package com.ronye.rota.repository;

import org.springframework.data.repository.CrudRepository;

import com.ronye.rota.domain.Rota;

public interface RotaRepository extends CrudRepository<Rota, Long> {

}

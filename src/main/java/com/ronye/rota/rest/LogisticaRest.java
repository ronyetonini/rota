package com.ronye.rota.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ronye.rota.domain.Mapa;
import com.ronye.rota.domain.Rota;
import com.ronye.rota.service.CalculoCusto;
import com.ronye.rota.service.MapaService;

@RestController
@RequestMapping("/services")
public class LogisticaRest {

	@Autowired
	private MapaService mapaService;

	private static final Logger logger = LoggerFactory.getLogger(LogisticaRest.class);

	@RequestMapping(value = "/cargaMapa", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> cargaMapa(@RequestBody MapaTO mapaTO) {

		if (mapaTO == null || mapaTO.getRotas() == null || mapaTO.getRotas().isEmpty() || mapaTO.getNome() == null || mapaTO.getNome().isEmpty()) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		
		logger.debug("Carregando Mapa " + mapaTO.getNome());

		List<RotaTO> rotasTO = mapaTO.getRotas();
		logger.debug("Total de " + mapaTO.getRotas().size() + " rotas");

		List<Rota> rotas = new ArrayList<Rota>();
		
		for (RotaTO rotaTO : rotasTO) {
			Rota rota = new Rota(rotaTO.getOrigem(), rotaTO.getDestino(), rotaTO.getDistancia());
			rotas.add(rota);
		}
		
		Mapa mapa = mapaService.adicionarMapa(rotas, mapaTO.getNome());
		
		logger.debug("Mapa cadastrado. ID: " + mapa.getId());
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/melhorRota", method = RequestMethod.GET)
	public @ResponseBody MelhorRota melhorRota(@RequestParam("mapa") String mapa, @RequestParam("origem") String origem, @RequestParam("destino") String destino, @RequestParam("autonomia") Double autonomia, @RequestParam("valorLitroCombustivel") Double valorLitroCombustivel) {
		
		MelhorRota melhorRota = new MelhorRota();
		List<Rota> menorRota = mapaService.obterMenorRota(mapa, origem, destino);
		if(menorRota.size() != 0){
			Integer distanciaTotal = 0;
			melhorRota.getPontos().add(menorRota.get(0).getOrigem());
			for (Rota rota : menorRota) {
				melhorRota.getPontos().add(rota.getDestino());
				distanciaTotal += rota.getDistancia();
			};
			
			CalculoCusto calculaCustoViagem = new CalculoCusto(valorLitroCombustivel, distanciaTotal, autonomia);
			melhorRota.setValor(calculaCustoViagem.getCustoTotal());
		}
		return melhorRota;
	}
}
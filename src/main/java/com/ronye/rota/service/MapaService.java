package com.ronye.rota.service;

import java.util.List;

import com.ronye.rota.domain.Mapa;
import com.ronye.rota.domain.Rota;

public interface MapaService {

	public Mapa adicionarMapa(List<Rota> rotas, String nome);

	public List<Rota> obterMenorRota(String mapa, String origem, String destino);

}

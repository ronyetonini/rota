package com.ronye.rota.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ronye.rota.algoritmo.dijkstra.DijkstraAlgorithm;
import com.ronye.rota.algoritmo.dijkstra.Edge;
import com.ronye.rota.domain.Mapa;
import com.ronye.rota.domain.Rota;
import com.ronye.rota.repository.MapaRepository;
import com.ronye.rota.repository.RotaRepository;

@Service
public class MapaServiceImpl implements MapaService {
	
	private static final Logger logger = LoggerFactory.getLogger(MapaServiceImpl.class);
	
	@Autowired
	private MapaRepository mapaRepository;

	@Autowired
	private RotaRepository rotaRepository;

	@Transactional
	public Mapa adicionarMapa(List<Rota> rotas, String nome) {
		Mapa mapa = mapaRepository.findByNome(nome);
		if(mapa != null){
			for (Rota rota : mapa.getRotas()) {
				rotaRepository.delete(rota);
			}
			mapaRepository.delete(mapa.getId());
		}
		
		mapa = new Mapa(nome, new HashSet<Rota>(rotas));
		
		mapaRepository.save(mapa);
		for (Rota rota : rotas) {
			rota.setMapa(mapa);
			rotaRepository.save(rota);
		}
		return mapa;
	}

	@Override
	public List<Rota> obterMenorRota(String mapa, String origem, String destino) {
		Mapa mapaAux = mapaRepository.findByNome(mapa);
		Set<Rota> rotas = mapaAux.getRotas();
		logger.info("Processo de cálculo de menor rota");

		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(rotas);
		dijkstra.execute(origem);
		LinkedList<Edge> path = dijkstra.getPathEdges(destino);	

		List<Rota> menorRota = new ArrayList<Rota>();
		if(path != null){
			for (Edge edge : path) {
				menorRota.add(edge.getRota());
			}
		}
		
		return menorRota;
	}
}

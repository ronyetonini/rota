CREATE TABLE mapa (
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NOT NULL,
  PRIMARY KEY (id));

CREATE TABLE rota (
  id INT NOT NULL AUTO_INCREMENT,
  origem VARCHAR(255) NOT NULL,
  destino VARCHAR(255) NOT NULL,
  distancia INT NOT NULL,
  mapa_id INT NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (mapa_id, origem, destino),
--  INDEX FK_ROTA__MAPA_idx (mapa_id ASC),
  CONSTRAINT FK_ROTA__MAPA
    FOREIGN KEY (mapa_id)
    REFERENCES mapa (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

CREATE INDEX IF NOT EXISTS FK_ROTA_1_idx
ON rota (origem, distancia);

CREATE INDEX IF NOT EXISTS FK_ROTA_2_idx
ON rota (destino, distancia);

INSERT INTO mapa (nome) VALUES ('SP');
INSERT INTO rota (origem, destino, distancia, mapa_id) VALUES ('A', 'B', 10, 1);
INSERT INTO rota (origem, destino, distancia, mapa_id) VALUES ('B', 'D', 15, 1);
INSERT INTO rota (origem, destino, distancia, mapa_id) VALUES ('A', 'C', 20, 1);
INSERT INTO rota (origem, destino, distancia, mapa_id) VALUES ('C', 'D', 30, 1);
INSERT INTO rota (origem, destino, distancia, mapa_id) VALUES ('B', 'E', 50, 1);
INSERT INTO rota (origem, destino, distancia, mapa_id) VALUES ('D', 'E', 30, 1);


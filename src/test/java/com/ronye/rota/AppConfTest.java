package com.ronye.rota;

import javax.transaction.Transactional;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.ronye.rota.App;

@SpringApplicationConfiguration(classes = App.class)
@ActiveProfiles("test")
@TransactionConfiguration(defaultRollback=true)
@Transactional
public class AppConfTest {

}

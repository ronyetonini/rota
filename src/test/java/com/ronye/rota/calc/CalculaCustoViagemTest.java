package com.ronye.rota.calc;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.ronye.rota.service.CalculoCusto;

public class CalculaCustoViagemTest {

	@Test
	public void deveRetornarCustoViagem() {
		Integer distancia = 100;
		Double valorLitro = 2.5;
		Double autonomia = 10.0;
		CalculoCusto calculaCustoViagem = new CalculoCusto(valorLitro, distancia, autonomia);
		Double custoTotal = calculaCustoViagem.getCustoTotal();
		assertTrue("Custo Total: " + custoTotal, custoTotal.equals(25.0));
	}
	
	@Test
	public void deveRetornarCustoViagemValoresDecimais() {
		Integer distancia = 33;
		Double valorLitro = 2.5;
		Double autonomia = 7.3;
		CalculoCusto calculaCustoViagem = new CalculoCusto(valorLitro, distancia, autonomia);
		Double custoTotal = calculaCustoViagem.getCustoTotal();
		assertTrue("Custo Total: " + custoTotal, custoTotal.equals(11.3));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void deveValidarParametroNulo() {
		Integer distancia = 33;
		Double valorLitro = null;
		Double autonomia = 7.3;
		CalculoCusto calculaCustoViagem = new CalculoCusto(valorLitro, distancia, autonomia);
		calculaCustoViagem.getCustoTotal();
	}
}

package com.ronye.rota.calc;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Sets;
import com.ronye.rota.algoritmo.dijkstra.DijkstraAlgorithm;
import com.ronye.rota.algoritmo.dijkstra.Edge;
import com.ronye.rota.domain.Rota;

public class DijkstraAlgorithmTest {

	@Test(expected=IllegalArgumentException.class)
	public void deveValidarParametrosNulos() {
		new DijkstraAlgorithm((Set<Rota>) null);
	}
	
	@Test
	public void menorCaminho1(){
		Set<Rota> rotas = Sets.newHashSet();
		rotas.add(new Rota(1l, "A", "B", 10));
		rotas.add(new Rota(2l, "B", "D", 15));
		rotas.add(new Rota(3l, "A", "C", 20));
		rotas.add(new Rota(4l, "C", "D", 30));
		rotas.add(new Rota(5l, "B", "E", 50));
		rotas.add(new Rota(6l, "D", "E", 30));

		String origem = "A";
		String destino = "D";
		
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(rotas);
		dijkstra.execute(origem);
		LinkedList<Edge> path = dijkstra.getPathEdges(destino);	

		List<Rota> menorRota = new ArrayList<Rota>();
		for (Edge edge : path) {
			menorRota.add(edge.getRota());
		}
		
		assertTrue(menorRota.get(0).getOrigem().equals("A") && menorRota.get(0).getDestino().equals("B"));
		assertTrue(menorRota.get(1).getOrigem().equals("B") && menorRota.get(1).getDestino().equals("D"));
	}

	@Test
	public void menorCaminho2() {
		String origem = "A";
		String destino = "F";
		Set<Rota> rotas = Sets.newHashSet();
		
		rotas.add(new Rota(1l, "A", "B", 5));
		rotas.add(new Rota(2l, "A", "C", 10));	
		rotas.add(new Rota(3l, "B", "D", 3));
		rotas.add(new Rota(4l, "B", "G", 10));	
		rotas.add(new Rota(5l, "C", "D", 10));
		rotas.add(new Rota(6l, "C", "E", 10));		
		rotas.add(new Rota(7l, "D", "F", 10));
		rotas.add(new Rota(8l, "D", "E", 5));
		rotas.add(new Rota(9l, "E", "F", 4));
		rotas.add(new Rota(10l, "E", "H", 1));
		rotas.add(new Rota(11l, "G", "F", 10));		
		rotas.add(new Rota(12l, "H", "F", 1));
				
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(rotas);
		dijkstra.execute(origem);
		LinkedList<Edge> path = dijkstra.getPathEdges(destino);	

		List<Rota> menorRota = new ArrayList<Rota>();
		for (Edge edge : path) {
			menorRota.add(edge.getRota());
		}
		
		assertTrue(menorRota.get(0).getOrigem().equals("A"));
		assertTrue(menorRota.get(0).getDestino().equals("B"));
		assertTrue(menorRota.get(1).getDestino().equals("D"));
		assertTrue(menorRota.get(2).getDestino().equals("E"));
		assertTrue(menorRota.get(3).getDestino().equals("H"));
		assertTrue(menorRota.get(4).getDestino().equals("F"));
	}
	
}


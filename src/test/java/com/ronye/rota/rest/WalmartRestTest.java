package com.ronye.rota.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.Assert;

import com.ronye.rota.AppConfWebTest;
import com.ronye.rota.domain.Rota;
import com.ronye.rota.rest.MapaTO;
import com.ronye.rota.rest.MelhorRota;
import com.ronye.rota.rest.RotaTO;
import com.ronye.rota.service.MapaService;

@RunWith(SpringJUnit4ClassRunner.class)
public class WalmartRestTest extends AppConfWebTest {
	
	@Autowired
	private MapaService mapaService;

	@Test
	public void cadastrarMalhaLogistica() throws Exception {

		MapaTO mapaTO = new MapaTO();
		mapaTO.setNome("MG");

		List<RotaTO> rotas = new ArrayList<RotaTO>();
		rotas.add(new RotaTO("A", "B", 27));
		rotas.add(new RotaTO("B", "C", 13));
		rotas.add(new RotaTO("C", "D", 30));

		mapaTO.setRotas(rotas);
		
		this.mvc.perform(
				post("/services/cargaMapa")
				.contentType(APPLICATION_JSON_UTF8)
				.content(gson.toJson((mapaTO))))
				.andExpect(status().isOk());
		
		mapaTO = new MapaTO();
		mapaTO.setNome("MG");

		rotas = new ArrayList<RotaTO>();
		rotas.add(new RotaTO("A", "B", 67));
		rotas.add(new RotaTO("B", "C", 43));
		rotas.add(new RotaTO("C", "D", 35));

		mapaTO.setRotas(rotas);
		
		this.mvc.perform(
				post("/services/cargaMapa")
				.contentType(APPLICATION_JSON_UTF8)
				.content(gson.toJson((mapaTO))))
				.andExpect(status().isOk());
		
	}
	
	@Test
	public void invalidarCadastroDeMapaSemNome() throws Exception {
		MapaTO mapaTO = new MapaTO();
		this.mvc.perform(
				post("/services/cargaMapa")
				.contentType(APPLICATION_JSON_UTF8)
				.content(gson.toJson((mapaTO))))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void invalidarCadastroDeMapaSemRota() throws Exception {
		MapaTO mapaTO = new MapaTO();
		mapaTO.setNome("Mapa zero");
		this.mvc.perform(
				post("/services/cargaMapa")
				.contentType(APPLICATION_JSON_UTF8)
				.content(gson.toJson((mapaTO))))
				.andExpect(status().isBadRequest());
	}
	
	@Test
	public void retornarmenorValorDeEntrega() throws Exception {
		popularRotas();
		
		MvcResult mvcReturn = this.mvc.perform(
				get("/services/melhorRota")
				.param("mapa", "TESTE")
				.param("origem", "A")
				.param("destino", "D")
				.param("autonomia", "10")
				.param("valorLitroCombustivel", "2.5")
				).andExpect(status().isOk())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8))
				.andReturn();
		
		MelhorRota resultado = gson.fromJson(mvcReturn.getResponse().getContentAsString(), MelhorRota.class);
		
		Assert.isTrue(resultado.getValor().equals(6.25));
		Assert.isTrue(resultado.getPontos().get(1).equals("B"));
	}
	
	private void popularRotas() {
		List<Rota> rotas = new ArrayList<Rota>();
		rotas.add(new Rota("A", "B", 10));
		rotas.add(new Rota("B", "D", 15));
		rotas.add(new Rota("A", "C", 20));
		rotas.add(new Rota("C", "D", 30));
		rotas.add(new Rota("B", "E", 50));
		rotas.add(new Rota("D", "E", 30));
		mapaService.adicionarMapa(rotas, "TESTE");
	}
}

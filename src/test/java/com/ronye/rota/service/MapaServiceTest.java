package com.ronye.rota.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ronye.rota.AppConfTest;
import com.ronye.rota.domain.Mapa;
import com.ronye.rota.domain.Rota;
import com.ronye.rota.repository.MapaRepository;
import com.ronye.rota.repository.RotaRepository;

@RunWith(SpringJUnit4ClassRunner.class)
public class MapaServiceTest extends AppConfTest {

	@Autowired
	private MapaService mapaService;

	@Autowired
	private MapaRepository mapaRepository;
	
	@Autowired
	private RotaRepository rotaRepository;

	@Test
	public void cargaMapa() {
		List<Rota> rotas = new ArrayList<Rota>();
		Mapa mapa = mapaService.adicionarMapa(rotas, "MapaTest");

		Mapa mapaBD = mapaRepository.findOne(mapa.getId());
		assertTrue(mapaBD.getNome().equals(mapa.getNome()));
	}

	
	@Test
	public void adicionarRotasNoMapa() {
		List<Rota> rotas = new ArrayList<Rota>();
		for (int i = 0; i < 50; i++) {
			rotas.add(new Rota(String.valueOf(i), String.valueOf(i+1), i+100));
		}
		mapaService.adicionarMapa(rotas, "MapaTest");

		Set<Rota> rotasBD = (Set<Rota>) mapaRepository.findByNome("MapaTest").getRotas();
		assertTrue(rotasBD.size() == rotas.size());
	}
	
	@Test
	public void menorRota() {
		List<Rota> rotas = new ArrayList<Rota>();
		
		rotas.add(new Rota(1l, "A", "B", 10));
		rotas.add(new Rota(2l, "B", "D", 15));
		rotas.add(new Rota(3l, "A", "C", 20));
		rotas.add(new Rota(4l, "C", "D", 30));
		rotas.add(new Rota(5l, "B", "E", 50));
		rotas.add(new Rota(6l, "D", "E", 30));
				
		mapaService.adicionarMapa(rotas, "SP");
		
		List<Rota> menorRota = mapaService.obterMenorRota("SP", "A", "D");
		
		assertTrue(menorRota.get(0).getOrigem().equals("A") && menorRota.get(0).getDestino().equals("B"));
		assertTrue(menorRota.get(1).getOrigem().equals("B") && menorRota.get(1).getDestino().equals("D"));
	}
}
